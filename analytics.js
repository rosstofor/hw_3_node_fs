const fs = require('fs')
const path = require('path')

let fullData = []

const getInfo = (type) => {
  const fileContent = fs.readFileSync(path.join(__dirname, './audit', `${type}.csv`)).toString()

  const parsedData = convertCSV2Array(fileContent)

  for (let i = 0; i < parsedData.length; i++) {
    let tmp = parsedData[i]
    tmp.push(type)
    fullData.push(tmp)
  }
}

const convertCSV2Array = (data, delimiter = ',', firstRow = false) => {
  return data
    .slice(firstRow ? data.indexOf('\n') + 1 : 0)
    .split('\n')
    .map((row) => row.split(delimiter))
}

const printData = (fullData) => {
  let arrObjects = []
  for (let i = 0; i < fullData.length; i++) {
    let user = {}
    user.date = fullData[i][0]
    user.value = fullData[i][1]
    user.name = fullData[i][2]
    user.type = fullData[i][3]
    arrObjects.push(user)
  }
  let users = {}

  arrObjects.forEach((el) => {
    if (typeof users[el.name] === 'undefined') {
      users[el.name] = []
    }
    users[el.name].push({ type: el.type, date: el.date, value: el.value })
  })

  Object.keys(users).forEach((user) => {
    console.log(`\nUser ${user} entred data ${users[user].length} times`)
    users[user].forEach((el) => {
      console.log(`${el['type']}: ${el['value']} (${el['date']})`)
    })
  })

  return arrObjects
}

getInfo('gas')
getInfo('water')
printData(fullData)
