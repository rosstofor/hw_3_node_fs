const fs = require('fs')
const path = require('path')

fs.watch(path.join(__dirname, './dist'), (eventType, fileName) => {
  if (eventType === 'change') {
    const watchFile = path.join(__dirname, './dist', fileName)
    const data = fs.readFileSync(watchFile).toString()
    const parsedData = JSON.parse(data)
    console.log(parsedData)
    switch (parsedData.type) {
      case 'gas':
        try {
          fs.appendFile(path.join(__dirname, './audit/gas.csv'), `\n${parsedData.date},${parsedData.value},${parsedData.name}`, () => {
            console.log('gas.csv added')
            fs.unlink(watchFile, () => {
              console.log(`${watchFile} deleted`)
            })
          })
        } catch (error) {
          console.log(error)
        }

        break
      case 'water':
        try {
          fs.appendFile(path.join(__dirname, './audit/water.csv'), `\n${parsedData.date},${parsedData.value},${parsedData.name}`, () => {
            console.log('water.csv added')
            fs.unlink(watchFile, () => {
              console.log(`${watchFile} deleted`)
            })
          })
        } catch (error) {
          console.log(error)
        }
        break
    }
  }
})
