const readLine = require('readline')
const fs = require('fs')
const path = require('path')

const rl = readLine.createInterface({
  output: process.stdout,
  input: process.stdin,
})

function askSync(q) {
  return new Promise((resolve, reject) => {
    rl.question(q, (data) => {
      if (!data) reject(new Error('Error'))
      resolve(data)
    })
  })
}
let data = {}

async function asker() {
  data.name = await askSync('Your name? ')
  if (data.name.length < 2 || data.name.length > 20) {
    console.log('Name name length must be within 2-20 characters')
    return process.exit()
  }
  data.value = await askSync('Value? ')
  if (data.value.length !== 6 || !parseInt(data.value)) {
    console.log('Value should be a NUMBER with 6 symbol')
    return process.exit()
  }
  let today = new Date()
  let dd = today.getDate()

  let mm = today.getMonth() + 1
  let yyyy = today.getFullYear()
  if (dd < 10) {
    dd = '0' + dd
  }

  if (mm < 10) {
    mm = '0' + mm
  }
  today = dd + '-' + mm + '-' + yyyy
  console.log(`Added current date ${today}`)

  data.date = today
  data.type = await askSync('Type?(gas/water) ')
  if (data.type !== 'gas' && data.type !== 'water') {
    console.log(`Only 'gas' or 'water'`)
    return process.exit()
  }

  fs.writeFileSync(path.join(__dirname, `./dist/${Date.now()}info.json`), JSON.stringify(data))
  rl.close()
}
asker()
